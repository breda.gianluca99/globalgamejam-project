using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTextObject : MonoBehaviour
{
    public bool Automatic = true;
    public List<Textline> textlines = new List<Textline>();


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out PlayerController _))
        {
            UITextManager.Instance.StartShowText(this);
            gameObject.SetActive(false);
        }
    }
}

[System.Serializable]
public class Textline
{
    public string text;
    public float duration;
    public bool activateAudio = true;
    public GenericEvent audio;
    public Color color = new Vector4(0,0,0,1);
}