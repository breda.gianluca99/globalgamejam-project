using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoMusicaLivello2 : DialogueTextObject
{
    public float tempoPrimaDiTastoGrigio;
    float timer;
    public float timeBeforeWhistling = 0;
    public bool whistle = false;
    private void OnValidate()
    {
        timeBeforeWhistling = 0;
        for (int i = 0; i < textlines.Count; i++)
        {
            timeBeforeWhistling += textlines[i].duration;
        }
    }

    private void Start()
    {
        UITextManager.Instance.StartShowText(this);
        timeBeforeWhistling = 0;
        for (int i = 0; i < textlines.Count; i++)
        {
            timeBeforeWhistling += textlines[i].duration;
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > tempoPrimaDiTastoGrigio)//2
        {
            //Debug.Log("TasutoGurigio");
            UITextManager.Instance.tasutoGurigio.SetActive(true);
        }

        if (timer > timeBeforeWhistling && whistle == false)//9
        {
            whistle = true;
            FmodManager.instance.GetComponent<BackgroundMusic>().Cappella(true);
        }
    }
}
