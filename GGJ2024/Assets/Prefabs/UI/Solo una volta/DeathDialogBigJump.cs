using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathDialogBigJump : DialogueTextObject
{
    public void StartTextline()
    {
        UITextManager.Instance.StartShowText(this);
    }
}
