using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static PlayerController;

public class TasutoGurigio : DialogueTextObject, IPointerClickHandler
{
    NoMusicaLivello2 t;
    public void OnPointerClick(PointerEventData eventData)
    {
        t = FindObjectOfType<NoMusicaLivello2>();
        t.whistle = true;

        UITextManager.Instance.StartShowText(this);
        gameObject.SetActive(false);
        FmodManager.instance.GetComponent<BackgroundMusic>().Cappella(false);
        //FmodManager.instance.GetComponent<BackgroundMusic>().StopPlatformMusic();
    }
}
