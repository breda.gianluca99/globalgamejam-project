using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FMODUnity;
using System.Security.Cryptography;

public class UITextManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI dialogueTMP;
    [SerializeField] FmodManager FM;
    public GameObject tasutoGurigio;

    bool textActive;
    DialogueTextObject textObject;
    int textLineIndex;
    float textTimer = 0;

    public static UITextManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        dialogueTMP.text = "";
        FM = FindObjectOfType<FmodManager>();
    }

    void Update()
    {
        if (textActive && textObject.Automatic)
        {
            textTimer += Time.deltaTime;
            if (textTimer > textObject.textlines[textLineIndex].duration)
            {
                ShowNextText();
            }
        }
    }

    //OnComplete
    //Interrupt
    public void SetTextObject(DialogueTextObject dto)
    {
        textObject = dto;
    }

    public void StartShowText(DialogueTextObject dto)
    {
        textActive = true;
        textObject = dto;
        textLineIndex = 0;
        textTimer = 0;
        dialogueTMP.text = textObject.textlines[textLineIndex].text;
        dialogueTMP.color = textObject.textlines[textLineIndex].color;
        if (textObject.textlines[textLineIndex].activateAudio)
        {
            string temp = textObject.textlines[textLineIndex].audio.EventPath.Guid.ToString();
            //FM.CreateGenericEventInstance(ref textObject.textlines[textLineIndex].audio);
            FM.PlaySoundOneShot(temp, transform.position);
        }

        //dialogueTMP.material.color = textObject.textlines[textLineIndex].color;
    }

    void ShowNextText()
    {
        textTimer = 0;
        if (textLineIndex < textObject.textlines.Count - 1)
        {
            textLineIndex++;
            dialogueTMP.text = textObject.textlines[textLineIndex].text;
            dialogueTMP.color = textObject.textlines[textLineIndex].color;
            if (textObject.textlines[textLineIndex].activateAudio)
            {
                string temp = textObject.textlines[textLineIndex].audio.EventPath.Guid.ToString();
                //FM.CreateGenericEventInstance(ref textObject.textlines[textLineIndex].audio);
                FM.PlaySoundOneShot(temp, transform.position);
            }
            //dialogueTMP.material.color = textObject.textlines[textLineIndex].color;
        }
        else
        {
            textActive = false;
            textLineIndex = 0;
            dialogueTMP.text = "";
        }
    }
}
/*
 *12s3
 *
 *2 death
 * big jump
 */
