using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShmupExplosion : MonoBehaviour
{
    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
