using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevel : MonoBehaviour
{
    [SerializeField] string nextStageName;
    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out PlayerController _))
        {
            FindObjectOfType<BlackEndLevel>().GetComponent<Image>().DOFade(1, 1);
            DOTween.Sequence().AppendInterval(1).OnComplete(() => FmodManager.instance.GetComponent<BackgroundMusic>().StopPlatformMusic());
            DOTween.Sequence().AppendInterval(1).OnComplete(() => SceneManager.LoadScene(nextStageName));
        }
    }
}
