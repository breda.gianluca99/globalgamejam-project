using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraShake : MonoBehaviour
{
    public float ShakeDuration;
    public float ShakeStrenght;
    float ShakeTimer;
    Vector3 t;
    private void Update()
    {
        Shake();
    }

    void Shake()
    {
        if (ShakeTimer > 0)
        {
            t = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), -10);
            transform.position = Vector3.Lerp(transform.position, t, ShakeStrenght * Time.deltaTime);
            ShakeTimer -= Time.deltaTime;
        }
        else
        {
            transform.position = new Vector3(0f, 0f, -10);
            ShakeTimer = 0;
        }
    }

    public void StartShake()
    {
        ShakeTimer = ShakeDuration;
    }
}
