using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TexturePan : MonoBehaviour
{
    protected enum BGDirection
    {
        up = 0, down, left, right
    }
    [SerializeField] private BGDirection Direction = BGDirection.up;
    private Vector2 backgroundDirection = Vector2.right;
    public Vector2 StartingOffset;
    public Vector4 StartingOffsetVector4;
    public float backgroundSpeed = 0f;
    private Renderer MeshRenderer;
    [Tooltip("If this is set then _MainTex reference of the shader will be ignored")]
    public string TextureOffsetNameProperty;

    private void OnValidate()
    {
        MeshRenderer = GetComponent<Renderer>();
        if (TextureOffsetNameProperty == "")
            MeshRenderer.sharedMaterial.mainTextureOffset = StartingOffset;
        else
            MeshRenderer.sharedMaterial.SetVector(TextureOffsetNameProperty, StartingOffsetVector4);

        switch (Direction)
        {
            case BGDirection.up:
                backgroundDirection = Vector2.up;
                break;
            case BGDirection.down:
                backgroundDirection = Vector2.down;
                break;
            case BGDirection.left:
                backgroundDirection = Vector2.left;
                break;
            case BGDirection.right:
                backgroundDirection = Vector2.right;
                break;
            default:
                break;
        }
    }

    void Update()
    {
        if (TextureOffsetNameProperty == "")
            MeshRenderer.sharedMaterial.mainTextureOffset += (backgroundDirection * backgroundSpeed * Time.deltaTime);
        else
            //return;
            MeshRenderer.sharedMaterial.
                SetVector(TextureOffsetNameProperty, MeshRenderer.sharedMaterial.
                GetVector(TextureOffsetNameProperty) +
                new Vector4(
                    (backgroundDirection * backgroundSpeed * Time.deltaTime).x,
                    (backgroundDirection * backgroundSpeed * Time.deltaTime).y));
    }

}
