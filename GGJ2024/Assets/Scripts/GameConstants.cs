public class GameConstants
{
    //tutte le constant string usate nel gioco per i comandi
    //pc specific
    public const string k_AxisNameVertical =               "Vertical";
    public const string k_AxisNameHorizontal =             "Horizontal";
    public const string k_MouseAxisNameVertical =          "Mouse Y";
    public const string k_MouseAxisNameHorizontal =        "Mouse X";
    public const string k_ButtonNameJump =                 "Jump";
    public const string k_ButtonNameFire =                 "Fire1";
    public const string k_ButtonNameSprint =               "Sprint";
    public const string k_ButtonNameCrouch =               "Crouch";
    public const string k_ButtonNameAim =                  "Fire2";
    public const string k_ButtonNameSwitchWeapon =         "Mouse ScrollWheel";
    public const string k_ButtonNameNextWeapon =           "NextWeapon";
    public const string k_ButtonNamePauseMenu =            "Pause Menu";
    public const string k_ButtonNameSubmit =               "Submit";
    public const string k_ButtonNameCancel =               "Cancel";

    //gamepad specific
    public const string k_AxisNameJoystickLookVertical =   "Look Y";
    public const string k_AxisNameJoystickLookHorizontal = "Look X";
    public const string k_ButtonNameGamepadFire =          "Gamepad Fire";
    public const string k_ButtonNameGamepadSwitchWeapon =  "Gamepad Switch";
    public const string k_ButtonNameGamepadAim =           "Gamepad Aim";

    // Audio const strings
    public static readonly string[] A_ElementAudio = { "Fire", "Spark", "Ice", "Boulder", "Darkness" };
}