using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Drawing;
using TMPro;
using UnityEngine.SocialPlatforms.Impl;

public class GameManagerShootemup : MonoBehaviour
{
    public SpaceShooterController player;
    public BoxCollider2D BoxColl;
    public TexturePan TexturePan;
    public EnemySpawner EnemySpawner;
    public GameObject VFX_Fall;
    public GameObject VFX_Rise;
    public GameObject VFX_Meteor;
    public Image Fade;
    public Save Save;
    public TextMeshProUGUI textScore;

    public FmodManager FM;
    public BackgroundMusic BGM;

    public int CurrentScore;
    public int ClearScore;

    public int NewSceneIndex;
    private void Start()
    {
        FM = FmodManager.instance;
        BGM = FM.GetComponent<BackgroundMusic>();
        BGM.StartShootemup();
    }
    private void Update()
    {
        textScore.text = "Score:" + CurrentScore;
    }

    public void AddScore()
    {
        CurrentScore++;
        if (CurrentScore >= ClearScore)
        {
            EndGame();
            Save.Score = CurrentScore;
        }
    }

    private void EndGame()
    {
        /*
         * rallento e accelero parallaxing con sinusoide da averso alto verso il basso
         * disattiva rotator
         * giocatore attiva rigidbody Launch into the air 
         * quando sta per cadere verso il basso dopo un certo ammontare di tempo attiva vfx meteora 
         * attiva vfx super speed dopo un certo ammontare di tempo 
         * aumento screenshake linearmente piu' tempo passo a "cadere"
         * player ri iniza a roteare super veloce
         * colpishe "prossimo stage" che sara' una collisione 2D in cui transizioneremo al prossimo livello
         * 
         * Musica Soundesigner
         */

        BGM = FM.GetComponent<BackgroundMusic>();

        BGM.EndShootemup();
        EnemySpawner.enabled = false;
        player.End();
        BoxColl.enabled = false;
        StopAndFall();

        BGM.manager.CreateGenericEventInstance(ref BGM._meteor);
        Sequence seqShake = DOTween.Sequence()
            .AppendInterval(1.75f)
            .OnComplete(() => BGM.manager.StartEvent(BGM._meteor));

        Sequence seqRise = DOTween.Sequence().AppendInterval(0.35f).OnComplete(() => VFX_Rise.SetActive(false));
        Sequence seqMeteor = DOTween.Sequence().AppendInterval(4.5f).OnComplete(() => VFX_Meteor.SetActive(true));
        Sequence seqFall = DOTween.Sequence().AppendInterval(3f).OnComplete(() => VFX_Fall.SetActive(true));

        Camera.main.GetComponent<CameraShake>().StartShake();

        Sequence seqFallComplete = DOTween.Sequence()
            .AppendInterval(10f)
            .Append(player.transform.DOMoveY(-100f, 3f))
            .AppendInterval(0.5f)
            .Append(Fade.DOFade(1f, 3f)
            .OnComplete(() => SceneManager.LoadScene(NewSceneIndex)));
    }

    private void StopAndFall()
    {

        Sequence seqBackground = DOTween.Sequence()
            .Append(DOTween.To(() => TexturePan.backgroundSpeed, x => TexturePan.backgroundSpeed = x, 0f, 1f).SetEase(Ease.OutCubic))
            .AppendInterval(0.25f)
            .Append(DOTween.To(() => TexturePan.backgroundSpeed, x => TexturePan.backgroundSpeed = x, -2.75f, 0.95f).SetEase(Ease.InCubic));

        Sequence seqPlayer = DOTween.Sequence()
            .Append(player.transform.DOMoveY(3.75f, 1.2f).SetEase(Ease.OutCubic))
            .AppendInterval(0.45f)
            .Append(player.transform.DOMoveY(0f, 0.8f).SetEase(Ease.InCubic));

        Sequence seqRot = DOTween.Sequence()
            .Append(DOTween.To(() => player.gameObject.GetComponent<Rotator>().rotSpeed, x => player.gameObject.GetComponent<Rotator>().rotSpeed = x, 50f, 1f).SetEase(Ease.OutCubic))
            .AppendInterval(1f)
            .Append(DOTween.To(() => player.gameObject.GetComponent<Rotator>().rotSpeed, x => player.gameObject.GetComponent<Rotator>().rotSpeed = x, 1500f, 0.7f).SetEase(Ease.OutCubic));

        Sequence seqShake = DOTween.Sequence()
            .AppendInterval(1f)
            .Append(DOTween.To(() => Camera.main.GetComponent<CameraShake>().ShakeStrenght, x => Camera.main.GetComponent<CameraShake>().ShakeStrenght = x, 30f, 12.5f).SetEase(Ease.OutCubic));
        // => tp.backgroundSpeed, x => tp.backgroundSpeed = x, -0.5f, 1);
    }
}
