using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;

public class SpaceShooterController : MonoBehaviour
{
    public GameObject LeftBoundary;
    public GameObject RightBoundary;
    public GameObject Projectile;
    public Transform Muzzle;
    public GameObject MuzzleVFX;
    public Rigidbody2D rb;

    public float speed = 2;
    private float firerate;
    public float Basefirerate = 0.2f;
    public bool end;
    private void Start()
    {
        firerate = Basefirerate;
    }
    private void Update()
    {
        if (end == false)
        {
            Shoot();
            Movement();
        }
    }
    public void End()
    {
        end = true;
        //rb.simulated = true;
        //rb.gravityScale = 1;
    }
    private void Shoot()
    {
        firerate -= Time.deltaTime;
        if (firerate <= 0f)
        {
            Instantiate(Projectile, Muzzle.position, Quaternion.identity);
            GameObject InstMuzzleVFX = Instantiate(MuzzleVFX, Muzzle);
            Destroy(InstMuzzleVFX, 2f);
            firerate = Basefirerate;
        }
    }

    #region Movement
    private void Movement()
    {
        float direction = Input.GetAxisRaw(GameConstants.k_AxisNameHorizontal);

        if (direction < 0)
        {
            MoveLeft();
        }
        else if (direction > 0)
        {
            MoveRight();
        }
    }
    private void MoveLeft()
    {
        transform.position += speed * Time.deltaTime * Vector3.left;
        //transform.rotation = Quaternion.Euler(25, 0, 0);
        if (transform.position.x <= LeftBoundary.transform.position.x)
        {
            transform.position = LeftBoundary.transform.position;
        }
    }

    private void MoveRight()
    {
        transform.position += speed * Time.deltaTime * Vector3.right;
        //transform.rotation = Quaternion.Euler(-25, 0, 0);
        if (transform.position.x >= RightBoundary.transform.position.x)
        {
            transform.position = RightBoundary.transform.position;
        }
    }
    #endregion


}
