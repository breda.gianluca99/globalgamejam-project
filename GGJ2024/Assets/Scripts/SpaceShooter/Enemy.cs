using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float Basefirerate;
    private float firerate;
    public float moveSpeed;
    public float projSpeed;
    public GameObject MuzzleVFX;
    public GameObject DeathVFX;
    public GameObject Projectile;
    public Transform Muzzle;
    public CapsuleCollider2D _col;
    public GameManagerShootemup GMS;

    [SerializeField] public delegate void OnDeath();
    public OnDeath death;

    // Start is called before the first frame update
    void Start()
    {
        _col = GetComponent<CapsuleCollider2D>();
        Destroy(transform.gameObject, 10f);
    }

    public void Death()
    {
        GMS.AddScore();
        Instantiate(DeathVFX, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= transform.up * (moveSpeed * Time.deltaTime);
        Shoot();
    }
    private void Shoot()
    {
        firerate -= Time.deltaTime;
        if (firerate <= 0f)
        {
            GameObject InstProjectile = Instantiate(Projectile, Muzzle.position, Muzzle.rotation);
            InstProjectile.GetComponent<EnemyProjectile>().speed = projSpeed;
            GameObject InstMuzzleVFX = Instantiate(MuzzleVFX, Muzzle.position, Muzzle.rotation);
            Destroy(InstMuzzleVFX, 2f);
            firerate = Basefirerate;
        }
    }
}