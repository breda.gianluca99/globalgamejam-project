using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public GameObject Rotat;
    public float rotSpeed = 1.0f;
    public Vector3 RotationWanted;
    private Vector3 rot;
    public Vector3 StartingRotation;
    private void OnValidate()
    {
        if (Rotat != null)
        StartingRotation = Rotat.transform.rotation.eulerAngles;
        if (RotationWanted.x > 1)
            RotationWanted.x = 1f;
        if (RotationWanted.x < 0)
            RotationWanted.x = 0f;

        if (RotationWanted.y > 1)
            RotationWanted.y = 1f;
        if (RotationWanted.y < 0)
            RotationWanted.y = 0f;

        if (RotationWanted.z > 1)
            RotationWanted.z = 1f;
        if (RotationWanted.z < 0)
            RotationWanted.z = 0f;
    }
    private void Start()
    {
        rot = StartingRotation;
    }

    void Update()
    {
        rot += RotationWanted * rotSpeed * Time.deltaTime;
        Rotat.transform.rotation = Quaternion.Euler(rot);
    }
}
