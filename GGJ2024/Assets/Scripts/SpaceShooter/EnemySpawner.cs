using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class EnemySpawner : MonoBehaviour
{
    [Header("Spawn Counter")]
    [SerializeField] private float maxSpawnCount = 1;

    [Header("Spawn Timer")]
    [SerializeField] private float minTimer;
    [SerializeField] private float maxTimer;

    [Header("Enemy Speed")]
    [SerializeField] private float minSpeed = 10f;
    [SerializeField] private float maxSpeed = 12f;
    private float randTimer;

    [Header("Enemy Weapon stats")]
    [SerializeField] float minBaseFireRate = 0.1f;
    [SerializeField] float maxBaseFireRate = 0.5f;
    [SerializeField] float minProjectileSpeed = 5f;
    [SerializeField] float maxProjectileSpeed = 20f;

    [Header("Spawn")]
    public GameObject LeftBoundary;
    public GameObject RightBoundary;
    public GameObject[] enemyPrefab;

    [Header("Rotation modifiers")]
    public float CurrentMaxRotationTimer;
    public float addedRotationRangeOnSpawn = 5f;
    public GameManagerShootemup GMS;

    private void Start()
    {
        randTimer = Random.Range(minTimer, maxTimer);
    }
    void Update()
    {
        Timer(randTimer);
    }
    private void OnValidate()
    {
        if (minTimer > maxTimer)
            maxTimer = minTimer;
        if (minSpeed > maxSpeed)
            maxSpeed = minSpeed;
        if (minBaseFireRate > maxBaseFireRate)
            maxBaseFireRate = minBaseFireRate;
        if (minProjectileSpeed > maxProjectileSpeed)
            maxProjectileSpeed = minProjectileSpeed;
    }
    private void Timer(float timeLeft)
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            randTimer = timeLeft;
        }

        if (timeLeft <= 0)
        {
            SpawnEnemy();
            randTimer = Random.Range(minTimer, maxTimer);
        }
    }

    private void SpawnEnemy()
    {
        CurrentMaxRotationTimer += addedRotationRangeOnSpawn;
        for (int i = 0; i < Random.Range(1, maxSpawnCount); i++)
        {
            float randomHeight = Random.Range(LeftBoundary.transform.position.x, RightBoundary.transform.position.x);
            Vector3 randomPos = new Vector3(randomHeight, Random.Range(transform.position.y, transform.position.y + 10f), transform.position.z);

            int randomEnemyType = Random.Range(0, enemyPrefab.Length);
            GameObject enemyInst = Instantiate(enemyPrefab[randomEnemyType]);

            enemyInst.GetComponent<Enemy>().moveSpeed = Random.Range(minSpeed, maxSpeed);
            enemyInst.GetComponent<Enemy>().Basefirerate = Random.Range(minBaseFireRate, maxBaseFireRate);
            enemyInst.GetComponent<Enemy>().projSpeed = Random.Range(minProjectileSpeed, maxProjectileSpeed);
            enemyInst.GetComponent<Enemy>().GMS = GMS;
            //enemyInst.GetComponent<Enemy>().death += GMS.AddScore();

            float randRot = Random.Range(-1 * CurrentMaxRotationTimer, CurrentMaxRotationTimer) * 5f;
            if (randRot < 0)
                randRot -= 50f;
            else
                randRot += 50f;
            enemyInst.GetComponent<Rotator>().rotSpeed = randRot;

            enemyInst.transform.position = randomPos;
        }
    }
}
