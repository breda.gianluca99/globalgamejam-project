using UnityEditor;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] public LayerMask Layer;
    [SerializeField] public GameObject Flag;
    public bool CustomRespawnPosition = false;
    public bool FirstSpawn = false;
    public Vector2 CustomPosition = Vector2.zero;
    private Vector2 RespawnPosition = Vector2.zero;
    private Vector2 DebugRespawnPosition = Vector2.zero;
    public float ZoffsetFlag = 1;
    private void Start()
    {
        RespawnPosition = GetPosition();
        if (FirstSpawn) return;
        else
            Instantiate(Flag, new Vector3(RespawnPosition.x, RespawnPosition.y, ZoffsetFlag), Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerController>())
        {
            other.gameObject.GetComponent<PlayerController>()._lastCheckpoint = RespawnPosition;
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        DebugRespawnPosition = GetPosition();
        DrawSphere(DebugRespawnPosition, Color.green);
        BoxCollider2D t = GetComponent<BoxCollider2D>();

        DrawBox(transform.position + new Vector3(t.offset.x, t.offset.y, 0), t.size * transform.localScale, Color.green);
    }

    void DrawSphere(Vector3 pos, Color color)
    {
        Handles.color = color;
        Handles.SphereHandleCap(-1, pos, Quaternion.identity, 0.5f, EventType.Repaint);
    }
    void DrawBox(Vector3 pos, Vector3 size, Color color)
    {
        Handles.color = color;
        Handles.DrawWireCube(pos, size);
    }
#endif
    private Vector3 GetPosition()
    {

        if (CustomRespawnPosition)
            RespawnPosition = CustomPosition + new Vector2(transform.position.x, transform.position.y);
        else
        {
            Physics2D.queriesStartInColliders = false;
            RaycastHit2D g = Physics2D.Raycast(transform.position, Vector2.down, 100f, Layer);
            if (g)
                RespawnPosition = g.point + new Vector2(0, 1f);
        }
        return RespawnPosition;
    }
}
