using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoombaStupido : MonoBehaviour
{
    [SerializeField] private float speed = -10;

    void Update()
    {
        transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
    }
}
