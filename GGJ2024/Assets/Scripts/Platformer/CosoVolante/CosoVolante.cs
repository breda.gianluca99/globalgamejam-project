using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosoVolante : MonoBehaviour
{
    [SerializeField] private float speed = -10;
    float currspeed;
    [SerializeField] private float timer = 2;
    float currentimer;
    [SerializeField] private bool isVertical = false;

    private void Start()
    {
        currspeed = speed;
    }

    void Update()
    {
        currentimer += Time.deltaTime;
        if (currentimer > timer)
        {
            currentimer = 0;
            currspeed = -currspeed;
        }
        if (!isVertical) transform.position = new Vector3(transform.position.x + currspeed * Time.deltaTime, transform.position.y, transform.position.z);
        else transform.position = new Vector3(transform.position.x, transform.position.y + currspeed * Time.deltaTime, transform.position.z);
    }
}
