using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DeathZone : MonoBehaviour
{
    public BoxCollider2D _col;
    public bool ToggleDeathTime = false;
    public float DeatTime = 2f;

    private void OnValidate()
    {
        _col = GetComponent<BoxCollider2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController tempPlayer = collision.gameObject.GetComponent<PlayerController>();

        DeathEffect(tempPlayer);

        if (ToggleDeathTime)
            StartCoroutine(DeathTimer(DeatTime, tempPlayer));
        else
        if (tempPlayer)
            Death(tempPlayer);
    }
    public IEnumerator DeathTimer(float time, PlayerController collision)
    {
        yield return new WaitForSeconds(time);
        Death(collision);
    }
    public void Death(PlayerController collision)
    {
        collision.Death();
    }
    public void DeathEffect(PlayerController collision)
    {
        collision.DeathEffect();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        DrawBox(transform.position + new Vector3(_col.offset.x, _col.offset.y, 0), _col.size * transform.localScale, Color.red);
    }
    void DrawBox(Vector3 pos, Vector3 size, Color color)
    {
        Handles.color = color;
        Handles.DrawWireCube(pos, size);
    }
#endif
}
