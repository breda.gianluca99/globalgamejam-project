using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChangeMusic : MonoBehaviour
{

    [SerializeField] private GenericEvent _musicToChangeInto;

    private BackgroundMusic BGM;
    private FmodManager FM;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BGM = FmodManager.instance.GetComponent<BackgroundMusic>();
        BGM.StartSnapshot(_musicToChangeInto);
        gameObject.SetActive(false);
    }

}
