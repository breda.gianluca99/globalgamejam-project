using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nemico : MonoBehaviour
{
    BoxCollider2D _col;
    [SerializeField] float headSize = 0.2f;
    [SerializeField] bool shouldReset = false;
    [SerializeField] bool CanRespawn = true;
    [SerializeField] FmodManager Fm;
    [SerializeField] GenericEvent _deathSound;

    float playerHalfHeight = 0.5f;

    Vector3 savePos = Vector3.zero;
    void Start()
    {
        Fm = FindObjectOfType<FmodManager>();
        _col = GetComponent<BoxCollider2D>();

        PlayerController.OnDeath += PlayerDiesHaHa;
        savePos = transform.position;
    }

    private void OnDestroy()
    {
        PlayerController.OnDeath -= PlayerDiesHaHa;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.transform.TryGetComponent(out PlayerController pc) && !collision.otherCollider.isTrigger)
        {
            float heightCheck = transform.position.y + _col.size.y * (0.5f - headSize);
            if (pc.gameObject.transform.position.y - playerHalfHeight > heightCheck)
                KillMyself_XD(pc);
            else
            {
                pc.DeathEffect();
                StartCoroutine(DeathTimer(1.5f, pc));
            }
        }
    }

    public IEnumerator DeathTimer(float time, PlayerController collision)
    {
        yield return new WaitForSeconds(time);
        collision.Death();
    }
    public void KillMyself_XD(PlayerController pc = null)
    {
        if (pc != null)
            pc.ExecuteJump();

        Fm.CreateGenericEventInstance(ref _deathSound);
        Fm.StartEvent(_deathSound);
        gameObject.SetActive(false);
    }

    void PlayerDiesHaHa()
    {
        if (shouldReset)
            transform.position = savePos;

        if (CanRespawn)
        {
            gameObject.SetActive(true);
            transform.rotation = Quaternion.identity;
        }
    }
}
