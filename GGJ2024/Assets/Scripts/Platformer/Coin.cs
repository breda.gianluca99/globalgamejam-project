using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject VFX;
    public GenericEvent Audio;
    internal void CoinGet()
    {
        if (VFX)
        {
            GameObject vfxinst = Instantiate(VFX, transform.position, Quaternion.identity);
            Destroy(vfxinst, 2f);
        }
        FmodManager.instance.PlaySoundOneShot(Audio.EventPath.Guid.ToString(), transform.position);
        Destroy(this.gameObject);
    }
}
