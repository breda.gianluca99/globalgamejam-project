using System;
using UnityEngine;
using TMPro;
using DG.Tweening;

/// <summary>
/// Hey!
/// Tarodev here. I built this controller as there was a severe lack of quality & free 2D controllers out there.
/// I have a premium version on Patreon, which has every feature you'd expect from a polished controller. Link: https://www.patreon.com/tarodev
/// You can play and compete for best times here: https://tarodev.itch.io/extended-ultimate-2d-controller
/// If you hve any questions or would like to brag about your score, come to discord: https://discord.gg/tarodev
/// </summary>

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class PlayerController : MonoBehaviour, IPlayerController
{
    public float TimeSinceDeath = 0;

    public TextMeshProUGUI textCoins;

    [SerializeField] private ScriptableStats _stats;
    public ScriptableStats Stats { get => _stats; set => _stats = value; }

    [SerializeField] private Save _save;
    public Save Save { get => _save; set => _save = value; }

    [SerializeField] private GameObject _deathEffect;

    [SerializeField] public GenericEvent _deathSound;

    //[Tooltip("da non toccare")]
    //[SerializeField] private Save _baseSave;
    //public Save BaseSave { get => _baseSave; }

    public Gun _gun;

    private Rigidbody2D _rb;
    private BoxCollider2D _col;
    //private CapsuleCollider2D _col;
    private FrameInput _frameInput;
    private Vector2 _frameVelocity;
    private bool _cachedQueryStartInColliders;
    public Vector3 _lastCheckpoint = Vector3.zero;
    public int Coins;
    public bool FireGun;

    #region Interface

    public Vector2 FrameInput => _frameInput.Move;
    public event Action<bool, float> GroundedChanged;//sostituibili con delegate
    public event Action Jumped;

    #endregion

    private float _time;

    //Oh no un delegate!!!

    //idk man lgtm as long as it works i guess

    public delegate void DiesInCringe();
    public static event DiesInCringe OnDeath;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        //_col = GetComponent<CapsuleCollider2D>();
        _col = GetComponent<BoxCollider2D>();
        _cachedQueryStartInColliders = Physics2D.queriesStartInColliders;
    }

    public void TogglePlayer(bool toggle)
    {
        _rb.simulated = toggle;
        CheckCollision = toggle;
        CheckInput = toggle;

    }

    private void Start()
    {
        if (Coins != Save.Coins)
            Coins = Save.Coins;
        textCoins.text = "Coins:" + Coins;
    }
    private void Update()
    {
        _time += Time.deltaTime;
        if (CheckInput)
        {
            GatherInput();
            HandleGun();
        }
    }
    public bool CheckInput;
    private void GatherInput()
    {
        _frameInput = new FrameInput
        {
            JumpDown = Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.C),
            JumpHeld = Input.GetButton("Jump") || Input.GetKey(KeyCode.C),
            Move = new Vector2(Input.GetAxisRaw("Horizontal"), 0)
        };

        FireGun = Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.X);

        if (_stats.SnapInput)
        {
            _frameInput.Move.x = Mathf.Abs(_frameInput.Move.x) < _stats.HorizontalDeadZoneThreshold ? 0 : Mathf.Sign(_frameInput.Move.x);
            _frameInput.Move.y = Mathf.Abs(_frameInput.Move.y) < _stats.VerticalDeadZoneThreshold ? 0 : Mathf.Sign(_frameInput.Move.y);
        }

        if (_frameInput.JumpDown)
        {
            _jumpToConsume = true;
            _timeJumpWasPressed = _time;
        }
    }

    public bool CheckCollision;

    private void FixedUpdate()
    {
        if (CheckCollision)
        {
            CheckCollisions();

            HandleJump();
            HandleDirection();
            HandleGravity();

            ApplyMovement();
        }
    }

    #region Collisions

    private float _frameLeftGrounded = float.MinValue;
    private bool _grounded;
    private void CheckCollisions()
    {
        Physics2D.queriesStartInColliders = false;

        // Ground and Ceiling
        RaycastHit2D t = Physics2D.BoxCast(_col.bounds.center, _col.size, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);
        bool groundHit = t;

        //Physics2D.CapsuleCast(_col.bounds.center, _col.size, CapsuleDirection2D.Vertical, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);
        //Physics2D.Raycast(_col.bounds.center, Vector2.down, _stats.GroundDeceleration, ~_stats.PlayerLayer);
        //Physics2D.BoxCast(_col.bounds.center, _col.size, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);
        //Physics2D.CapsuleCast(_col.bounds.center, _col.size, _col.direction, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);
        //Debug.DrawLine(_col.bounds.center,transform.position + Vector3.down * _stats.GrounderDistance);


        bool ceilingHit =
        //Physics2D.CapsuleCast(_col.bounds.center, _col.size, CapsuleDirection2D.Vertical, 0, Vector2.up, _stats.GrounderDistance, ~_stats.PlayerLayer);
        //Physics2D.Raycast(_col.bounds.center, Vector2.up, _stats.GroundDeceleration, ~_stats.PlayerLayer);
        Physics2D.BoxCast(_col.bounds.center, _col.size, 0, Vector2.up, _stats.GrounderDistance, ~_stats.PlayerLayer);
        //Physics2D.CapsuleCast(_col.bounds.center, _col.size, _col.direction, 0, Vector2.up, _stats.GrounderDistance, ~_stats.PlayerLayer);

        // Hit a Ceiling
        if (ceilingHit) _frameVelocity.y = Mathf.Min(0, _frameVelocity.y);

        // Landed on the Ground
        if (!_grounded && groundHit)
        {
            _grounded = true;
            _coyoteUsable = true;
            _bufferedJumpUsable = true;
            _endedJumpEarly = false;
            GroundedChanged?.Invoke(true, Mathf.Abs(_frameVelocity.y));
        }
        // Left the Ground
        else if (_grounded && !groundHit)
        {
            _grounded = false;
            _frameLeftGrounded = _time;
            GroundedChanged?.Invoke(false, 0);
        }

        Physics2D.queriesStartInColliders = _cachedQueryStartInColliders;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Coin"))
        {
            Coins++;
            _save.Coins = Coins;
            textCoins.text = "Coins:" + Coins;
            collision.GetComponent<Coin>().CoinGet();
        }

    }

    public void ResetSave()
    {
        _save.Coins = 0;
        _save.Score = 0;
    }

    #endregion

    #region Jumping

    private bool _jumpToConsume;
    private bool _bufferedJumpUsable;
    private bool _endedJumpEarly;
    private bool _coyoteUsable;
    private float _timeJumpWasPressed;

    private bool HasBufferedJump => _bufferedJumpUsable && _time < _timeJumpWasPressed + _stats.JumpBuffer;
    private bool CanUseCoyote => _coyoteUsable && !_grounded && _time < _frameLeftGrounded + _stats.CoyoteTime;


    private void HandleJump()
    {
        if (!_endedJumpEarly && !_grounded && !_frameInput.JumpHeld && _rb.velocity.y > 0) _endedJumpEarly = true;

        if (!_jumpToConsume && !HasBufferedJump) return;

        if (_grounded || CanUseCoyote) ExecuteJump();

        _jumpToConsume = false;
    }

    public void ExecuteJump()
    {
        _endedJumpEarly = false;
        _timeJumpWasPressed = 0;
        _bufferedJumpUsable = false;
        _coyoteUsable = false;
        _frameVelocity.y = _stats.JumpPower;
        Jumped?.Invoke();
    }

    #endregion

    #region Horizontal

    private void HandleDirection()
    {
        if (_frameInput.Move.x == 0)
        {
            var deceleration = _grounded ? _stats.GroundDeceleration : _stats.AirDeceleration;
            _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, 0, deceleration * Time.fixedDeltaTime);
        }
        else
        {
            _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, _frameInput.Move.x * _stats.MaxSpeed, _stats.Acceleration * Time.fixedDeltaTime);
        }
    }

    #endregion

    #region Gravity

    private void HandleGravity()
    {
        if (_grounded && _frameVelocity.y <= 0f)
        {
            _frameVelocity.y = _stats.GroundingForce;
        }
        else
        {
            var inAirGravity = _stats.FallAcceleration;
            if (_endedJumpEarly && _frameVelocity.y > 0) inAirGravity *= _stats.JumpEndEarlyGravityModifier;
            _frameVelocity.y = Mathf.MoveTowards(_frameVelocity.y, -_stats.MaxFallSpeed, inAirGravity * Time.fixedDeltaTime);
        }
    }
    #endregion

    private void ApplyMovement() => _rb.velocity = _frameVelocity;

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_stats == null) Debug.LogWarning("Please assign a ScriptableStats asset to the Player Controller's Stats slot", this);
    }
#endif

    #region Gun
    public float rotation;
    private void HandleGunRotate()
    {
        rotation = _gun.transform.rotation.eulerAngles.y;

        if (FrameInput.x < 0 && rotation != 180f)
            _gun.transform.rotation = Quaternion.Euler(transform.rotation.x, 180f, transform.rotation.z);
        else if (FrameInput.x > 0 && rotation != 0f)
            _gun.transform.rotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
    }

    private void HandleGun()
    {
        HandleGunRotate();
        if (FireGun)
        {
            ShootGun();
        }
    }
    private void ShootGun()
    {
        _gun.Shoot();
    }

    #endregion

    public void Death()
    {
        TimeSinceDeath = _time;
        //DeathCounter++;
        transform.position = _lastCheckpoint;

        GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;

        CheckInput = true;
        CheckCollision = true;
        _rb.simulated = true;

        OnDeath?.Invoke();
    }
    public void DeathEffect()
    {
        GameObject t = Instantiate(_deathEffect, transform.position + Vector3.up, Quaternion.identity);
        Destroy(t, 1.5f);

        FmodManager.instance.CreateGenericEventInstance(ref _deathSound);

        DOTween.Sequence().AppendInterval(0.15f).OnComplete(() => FmodManager.instance.StartEvent(_deathSound));

        GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;

        CheckInput = false;
        CheckCollision = false;
        _rb.simulated = false;
    }
}

public struct FrameInput
{
    public bool JumpDown;
    public bool JumpHeld;
    public Vector2 Move;
}

public interface IPlayerController
{
    public event Action<bool, float> GroundedChanged;

    public event Action Jumped;
    public Vector2 FrameInput { get; }
}