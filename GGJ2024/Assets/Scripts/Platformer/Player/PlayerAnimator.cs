using FMOD.Studio;
using FMODUnity;
using UnityEngine;


namespace TarodevController
{
    /// <summary>
    /// VERY primitive animator example.
    /// </summary>
    public class PlayerAnimator : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private Animator _anim;
        public Animator Anim { get => _anim; set => _anim = value; }

        [SerializeField] private SpriteRenderer _sprite;
        [SerializeField] private GameObject _mesh;

        [Header("Settings")]
        [SerializeField, Range(1f, 3f)]
        private float _maxIdleSpeed = 2;

        [SerializeField] private float _maxTilt = 5;
        [SerializeField] private float _tiltSpeed = 20;

        [Header("Particles")][SerializeField] private ParticleSystem _jumpParticles;
        [SerializeField] private ParticleSystem _launchParticles;
        [SerializeField] private ParticleSystem _moveParticles;
        [SerializeField] private ParticleSystem _landParticles;

        [Header("Audio Clips")]
        [SerializeField] private GenericEvent _jumpSound;
        [SerializeField] private GenericEvent _megaJump;

        public FmodManager _source;
        public IPlayerController _player;
        private bool _grounded;
        private ParticleSystem.MinMaxGradient _currentGradient;

        private void Awake()
        {
            _source = FindObjectOfType<FmodManager>();
            _player = GetComponentInParent<IPlayerController>();
            //for (int i = 0; i < _footsteps.Length; i++)//crea array di tutti gli eventi dela array _footsteps
            //{
            //    _source.CreateGenericEventInstance(ref _footsteps[i]);
            //}
            _source.CreateGenericEventInstance(ref _jumpSound);
            _source.CreateGenericEventInstance(ref _megaJump);

        }

        private void OnEnable()
        {
            _player.Jumped += OnJumped;
            _player.GroundedChanged += OnGroundedChanged;

            _moveParticles.Play();
        }

        private void OnDisable()
        {
            _player.Jumped -= OnJumped;
            _player.GroundedChanged -= OnGroundedChanged;

            _moveParticles.Stop();
        }

        private void Update()
        {
            if (_player == null) return;

            DetectGroundColor();

            HandleSpriteFlip();

            HandleIdleSpeed();

            //HandleCharacterTilt();

            HandleMeshFlip();
        }

        private void HandleSpriteFlip()
        {
            if (_player.FrameInput.x != 0) _sprite.flipX = _player.FrameInput.x < 0;
        }

        private void HandleMeshFlip()
        {
            if (_player.FrameInput.x < 0)
                //_mesh.transform.localScale = new Vector3(1, 1, 1);
                Anim.transform.rotation = Quaternion.Euler(new Vector3(0, 155f, 0));

            else if (_player.FrameInput.x > 0)
                //_mesh.transform.localScale = new Vector3(-1, 1, 1);
                Anim.transform.rotation = Quaternion.Euler(new Vector3(0, 25f, 0));

        }

        private void HandleIdleSpeed()
        {
            var inputStrength = Mathf.Abs(_player.FrameInput.x);
            Anim.SetFloat(IdleSpeedKey, Mathf.Lerp(1, _maxIdleSpeed, inputStrength));
            _moveParticles.transform.localScale = Vector3.MoveTowards(_moveParticles.transform.localScale, Vector3.one * inputStrength, 2 * Time.deltaTime);
        }

        private void HandleCharacterTilt()
        {
            var rot = Anim.transform.rotation.eulerAngles;
            var runningTilt = _grounded ? Quaternion.Euler(rot.x, rot.y, rot.z * _maxTilt * _player.FrameInput.x) : Quaternion.identity;
            Anim.transform.up = Vector3.RotateTowards(Anim.transform.up, runningTilt * Vector2.up, _tiltSpeed * Time.deltaTime, 0f);
        }

        public void OnJumped()
        {
            Anim.SetTrigger(JumpKey);
            Anim.ResetTrigger(GroundedKey);

            _source.StartEvent(_jumpSound);

            //._source.StartEvent(ref _megaJump)

            //Player.GetComponent<PlayerAnimator>()._source.CreateGenericEventInstance(ref _megaJump);
            //Player.GetComponent<PlayerAnimator>()._source.StartEvent(_megaJump);

            if (_grounded) // Avoid coyote
            {
                SetColor(_jumpParticles);
                SetColor(_launchParticles);
                _jumpParticles.Play();
            }
        }
        public void OnMegaJumped()
        {
            _player.Jumped -= OnJumped;
            _source.StartEvent(_megaJump);
        }

        private void OnGroundedChanged(bool grounded, float impact)
        {
            _grounded = grounded;

            if (grounded)
            {
                DetectGroundColor();
                SetColor(_landParticles);
                Anim.SetTrigger(GroundedKey);

                //_source.PlaySoundOneShot(_footsteps[Random.Range(0, _footsteps.Length)].EventPath.ToString(),transform.gameObject);
                //_source.
                //(_footsteps[Random.Range(0, _footsteps.Length)]);
                _moveParticles.Play();


                _landParticles.transform.localScale = Vector3.one * Mathf.InverseLerp(0, 40, impact);
                _landParticles.Play();
                //Debug.LogError("ground");
            }
            else
            {
                _moveParticles.Stop();
            }
        }

        private void DetectGroundColor()
        {
            var hit = Physics2D.Raycast(transform.position, Vector3.down, 2);

            if (!hit || hit.collider.isTrigger || !hit.transform.TryGetComponent(out SpriteRenderer r)) return;
            var color = r.color;
            _currentGradient = new ParticleSystem.MinMaxGradient(color * 0.9f, color * 1.2f);
            SetColor(_moveParticles);
        }

        private void SetColor(ParticleSystem ps)
        {
            var main = ps.main;
            main.startColor = _currentGradient;
        }

        private static readonly int GroundedKey = Animator.StringToHash("Grounded");
        private static readonly int IdleSpeedKey = Animator.StringToHash("IdleSpeed");
        private static readonly int JumpKey = Animator.StringToHash("Jump");

    }
}