using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public FmodManager Fm;
    public GameObject Projectile;
    public Transform Muzzle;
    [SerializeField]
    private GenericEvent _gunSound;
    public GameObject MuzzleVFX;
    public float speed = 2;
    public float Basefirerate = 0.2f;
    float firerate;
    bool fire;
    public void Start()
    {
        Fm = FindObjectOfType<FmodManager>();

        firerate = Basefirerate;
    }
    private void Update()
    {
        firerate -= Time.deltaTime;
        if (fire && firerate <= 0f)
        {
            GameObject InstProj = Instantiate(Projectile, Muzzle.position, Muzzle.rotation);
            GameObject InstMuzzleVFX = Instantiate(MuzzleVFX, Muzzle);
            Destroy(InstMuzzleVFX, 2f);
            InstProj.GetComponent<Projectile>().speed = speed;
            firerate = Basefirerate;
            fire = false;
            DOTween.Sequence()
            .Append(transform.DORotate(transform.rotation.eulerAngles + new Vector3(0, 0, 35f), 0.1f))
            .Append(transform.DORotate(transform.rotation.eulerAngles + new Vector3(0, 0, 0f), 0.05f));
            Fm.CreateGenericEventInstance(ref _gunSound);
            Fm.StartEvent(_gunSound);
        }
    }

    private IEnumerator GunshotSound(float t)
    {
        yield return null;
    }
    public void Shoot()
    {
        fire = true;
    }
}
