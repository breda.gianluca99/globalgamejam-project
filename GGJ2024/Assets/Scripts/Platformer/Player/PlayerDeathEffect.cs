using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystemRenderer DE;
    [SerializeField] private Material t;

    void Start()
    {
        DE.material = Instantiate(t);
        DE.material.DOFloat(1, "Alpha", 0.5f).OnComplete(() => DE.material.DOFloat(0, "Alpha", 0.5f));
    }
}
