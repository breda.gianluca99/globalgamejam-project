using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodCamera : MonoBehaviour
{
    [SerializeField] GameObject focus;

    [Header("Boundaries")]
    [SerializeField] float Lower = -999;
    [SerializeField] float Upper = 999;
    [SerializeField] float Left = -999;
    [SerializeField] float Right = 999;

    // Update is called once per frame
    void Update()
    {
        Vector3 _pos = focus.transform.position;
        if (_pos.x < Left) _pos.x = Left;
        if (_pos.x > Right) _pos.x = Right;
        if (_pos.y < Lower) _pos.y = Lower;
        if (_pos.y > Upper) _pos.y = Upper;

        transform.position = _pos;
    }
}
