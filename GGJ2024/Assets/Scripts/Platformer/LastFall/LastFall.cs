using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TarodevController;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class LastFall : MonoBehaviour
{

    [SerializeField] BoxCollider2D _col;

    [Header("VFX")]
    [SerializeField] GameObject VFX_player;
    [SerializeField] GameObject VFX_Enemy2;
    [SerializeField] GameObject VFX_Enemy;
    [SerializeField] GameObject VFX_GrassTop;
    [SerializeField] GameObject VFX_PlatformBase;
    [SerializeField] GameObject VFX_Coins;
    
    [Header("Setup")]
    [SerializeField] PlayerController Player;
    [SerializeField] Rotator RotatorPlayer;
    [SerializeField] FmodManager FM;
    [SerializeField] BackgroundMusic BGM;

    [Header("Texture Pan")]
    [SerializeField] TexturePan TP1;
    [SerializeField] TexturePan TP2;

    [Header("UI Images")]
    [SerializeField] Image IMG1;
    [SerializeField] Image IMG2;
    [SerializeField] Image FADE;

    [Header("Cameras")]
    [SerializeField] Camera Face;
    [SerializeField] Camera Face2;
    [SerializeField] Camera Ass;
    [SerializeField] Camera Transition;
    [SerializeField] Camera MainCam;

    [Header("Transition")]
    [SerializeField] Transform TransitionTransform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        FM = FindObjectOfType<FmodManager>();
        BGM = FM.GetComponent<BackgroundMusic>();
        Player = collision.gameObject.GetComponent<PlayerController>();
        RotatorPlayer = Player.GetComponent<Rotator>();
        BGM.StartEndFall();

        TogglePlayer(false);
        Player.transform.DOMove(transform.position, 0.75f);
        TP1.backgroundSpeed = 2;
        TP2.backgroundSpeed = 2;


        DOTween.Sequence().AppendInterval(6f).OnComplete(() => VFX_player.SetActive(true));
        DOTween.Sequence().AppendInterval(6f).OnComplete(() => VFX_player.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(6f).OnComplete(() => VFX_player.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(6f).OnComplete(() => VFX_player.transform.GetChild(2).gameObject.SetActive(true));

        DOTween.Sequence().AppendInterval(12f).OnComplete(() => VFX_Enemy2.SetActive(true));
        DOTween.Sequence().AppendInterval(12f).OnComplete(() => VFX_Enemy2.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(12f).OnComplete(() => VFX_Enemy2.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(12f).OnComplete(() => VFX_Enemy2.transform.GetChild(2).gameObject.SetActive(true));

        DOTween.Sequence().AppendInterval(18f).OnComplete(() => VFX_Enemy.SetActive(true));
        DOTween.Sequence().AppendInterval(18f).OnComplete(() => VFX_Enemy.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(18f).OnComplete(() => VFX_Enemy.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(18f).OnComplete(() => VFX_Enemy.transform.GetChild(2).gameObject.SetActive(true));

        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_GrassTop.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_GrassTop.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_GrassTop.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_GrassTop.transform.GetChild(2).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_PlatformBase.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_PlatformBase.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_PlatformBase.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(24f).OnComplete(() => VFX_PlatformBase.transform.GetChild(2).gameObject.SetActive(true));

        DOTween.Sequence().AppendInterval(30).OnComplete(() => VFX_Coins.SetActive(true));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => VFX_Coins.transform.GetChild(0).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => VFX_Coins.transform.GetChild(1).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => VFX_Coins.transform.GetChild(2).gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => IMG1.DOFade(1f, 6f));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => IMG2.DOFade(1f, 6f));

        DOTween.Sequence().AppendInterval(30).OnComplete(() => MainCam.gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(30).OnComplete(() => Transition.gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(30)
            .Append(Transition.transform.DOLocalMove(TransitionTransform.transform.localPosition, 6f))
            .Join(Transition.transform.DOLocalRotate(TransitionTransform.transform.localRotation.eulerAngles, 6f));
        DOTween.Sequence().AppendInterval(35.7f).OnComplete(() => FadeInFadeOut());
        DOTween.Sequence().AppendInterval(35.7f).OnComplete(() => RotatorPlayer.rotSpeed = 0);

        //DOTween.Sequence().AppendInterval(35).Append(Transition.transform.DOLocalMove(Face2.transform.localPosition,1f)).Join(Transition.transform.DOLocalRotate(Face2.transform.localRotation.eulerAngles,1f));

        //DOTween.Sequence().AppendInterval(36).OnComplete(() => MainCam.gameObject.SetActive(false));

        DOTween.Sequence().AppendInterval(36).OnComplete(() => Transition.gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(36).OnComplete(() => Face2.gameObject.SetActive(true));

        //FadeInFadeOut();
        DOTween.Sequence().AppendInterval(41.7f).OnComplete(() => FadeInFadeOut());
        DOTween.Sequence().AppendInterval(42).OnComplete(() => Face2.gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(42).OnComplete(() => Face.gameObject.SetActive(true));

        //FadeInFadeOut();
        DOTween.Sequence().AppendInterval(47.7f).OnComplete(() => FadeInFadeOut());
        DOTween.Sequence().AppendInterval(48).OnComplete(() => Face.gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(48).OnComplete(() => Ass.gameObject.SetActive(true));

        //FadeInFadeOut();
        DOTween.Sequence().AppendInterval(53.7f).OnComplete(() => FadeInFadeOut());
        DOTween.Sequence().AppendInterval(54).OnComplete(() => Ass.gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(54).OnComplete(() => MainCam.gameObject.SetActive(true));
        DOTween.Sequence().AppendInterval(54).OnComplete(() => RotatorPlayer.rotSpeed = 200);

        //DOTween.Sequence().AppendInterval(54).OnComplete(() => MainCam.gameObject.SetActive(true));
        

        DOTween.Sequence().AppendInterval(54).OnComplete(() => IMG1.DOFade(0f, 4f));
        DOTween.Sequence().AppendInterval(54).OnComplete(() => IMG2.DOFade(0f, 4f));

        DOTween.Sequence().AppendInterval(60).OnComplete(
            () => Player.GetComponent<PlayerAnimator>().Anim.transform.rotation = 
            Quaternion.Euler(
                Player.GetComponent<PlayerAnimator>().Anim.transform.rotation.eulerAngles.x, 
                Player.GetComponent<PlayerAnimator>().Anim.transform.rotation.eulerAngles.y, 0));

        DOTween.Sequence().AppendInterval(60).OnComplete(() => TogglePlayer(true));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => TP1.backgroundSpeed = 0);
        DOTween.Sequence().AppendInterval(60).OnComplete(() => TP2.backgroundSpeed = 0);

        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_player.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy2.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_GrassTop.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_PlatformBase.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Coins.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => _col.enabled = false);

        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Coins.transform.GetChild(0).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_PlatformBase.transform.GetChild(0).gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_GrassTop.transform.GetChild(0).gameObject.    SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy.transform.GetChild(0).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_player.transform.GetChild(0).gameObject.      SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy2.transform.GetChild(0).gameObject.      SetActive(false));

        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Coins.transform.GetChild(        1).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_PlatformBase.transform.GetChild( 1).gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_GrassTop.transform.GetChild(     1).gameObject.    SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy.transform.GetChild(        1).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_player.transform.GetChild(       1).gameObject.      SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy2.transform.GetChild(       1).gameObject.      SetActive(false));

        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Coins.transform.GetChild(        2).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_PlatformBase.transform.GetChild( 2).gameObject.SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_GrassTop.transform.GetChild(     2).gameObject.    SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy.transform.GetChild(        2).gameObject.       SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_player.transform.GetChild(       2).gameObject.      SetActive(false));
        DOTween.Sequence().AppendInterval(60).OnComplete(() => VFX_Enemy2.transform.GetChild(       2).gameObject.      SetActive(false));

        //I know you've been through a lot in this game
        //but trust me these guys went through a lot more to get this done
        //anyway they gave me this fatass like holy shi-
    }
    public void FadeInFadeOut()
    {
        DOTween.Sequence().Append(FADE.DOFade(1f, 0.3f)).Append(FADE.DOFade(0f, 0.3f));
    }


    public void TogglePlayer(bool toggle)
    {
        RotatorPlayer.enabled = !toggle;
        Player.TogglePlayer(toggle);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        DrawBox(transform.position + new Vector3(_col.offset.x, _col.offset.y, 0), _col.size * transform.localScale, Color.magenta);
    }
    void DrawBox(Vector3 pos, Vector3 size, Color color)
    {
        Handles.color = color;
        Handles.DrawWireCube(pos, size);
    }
#endif

    /*
     * giocatore cade
     * hitta il collider
     * stoppa il player
     * inizio ad aumentare il glitch effect ai lati piu' tempo passa
     * stop platfor music
     * start fall into code music
     * giocatore comicia a ruotare su tutti i 3 assi
     * oggetti cominciano ad appararire che cadono e ruotano o anche glitchano o anche spariscono e ricompaiono o anche tremano
     * ruotare tutti i vfx su la asse Y
     * random scale i vfx in random times
     * glitched vfx(?)
     */

}
