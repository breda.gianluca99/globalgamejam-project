using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoombaFurbo : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private ScriptableStats _stats;
    [SerializeField] private float activityDistance;

    Vector3 _frameVelocity;
    bool _grounded;
    bool _wallHit;
    PlayerController _playerController;
    Vector2 _playerPos;

    BoxCollider2D _col;

    private void Start()
    {
        _col = GetComponent<BoxCollider2D>();
        _frameVelocity.x = speed;

        _playerController = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        Physics2D.queriesStartInColliders = true;
        _grounded = Physics2D.BoxCast(_col.bounds.center, _col.size, 0, Vector2.down, _stats.GrounderDistance, ~_stats.PlayerLayer);

        CheckWallHit();

        _playerPos = _playerController.transform.position;
        if (Mathf.Abs(transform.position.x - _playerPos.x) < activityDistance)
        {
            ApplyGravity();
            transform.position += _frameVelocity * Time.deltaTime;
        }
    }

    void CheckWallHit()
    {
        Vector2 myDir = _frameVelocity.x < 0 ? Vector2.left : Vector2.right;
        _wallHit = Physics2D.BoxCast(_col.bounds.center, new Vector2 (_col.size.x, _col.size.y /2), 0, myDir, _stats.GrounderDistance, ~_stats.PlayerLayer);
        if (_wallHit )
        {
            _frameVelocity.x = -_frameVelocity.x;
        }
    }

    void ApplyGravity()
    {
        if (_grounded && _frameVelocity.y <= 0f)
        {
            _frameVelocity.y = 0;
        }
        else
        {
            var inAirGravity = _stats.FallAcceleration;
            _frameVelocity.y = Mathf.MoveTowards(_frameVelocity.y, -_stats.MaxFallSpeed, inAirGravity * Time.fixedDeltaTime);
        }
    }
}
