using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class CosiCerchio : MonoBehaviour
{
    [SerializeField] GameObject Coso;
    [SerializeField] int numberOfCosi;
    [SerializeField] float radius;
    public float speed;
    [SerializeField] bool isCounterClockwise;

    List<GameObject> myCosi = new List<GameObject>();
    List<Vector3> cosiStartPos = new List<Vector3>();

    void Start()
    {
        int n = numberOfCosi;
        for (int i = 0; i < n; i++)
        {
            float alp = math.PI * 2 * i / n;
            GameObject newCoso = Instantiate(Coso);
            newCoso.transform.SetParent(transform);
            newCoso.transform.localPosition = new Vector3(radius * math.cos(alp), radius * math.sin(alp), 0);
            newCoso.GetComponent<CosoInCerchio>().cerchio = this;
        }
    }

    void Update()
    {
        if (isCounterClockwise) { speed = -speed; } 
        transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
    }
}
