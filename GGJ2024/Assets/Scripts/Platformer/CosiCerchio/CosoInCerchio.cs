using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosoInCerchio : MonoBehaviour
{
    public CosiCerchio cerchio;

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, -cerchio.speed * Time.deltaTime));
    }
}
