using DG.Tweening;
using System.Collections;
using TarodevController;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSetter : MonoBehaviour
{
    [SerializeField] private ScriptableStats statsToSet;
    [SerializeField] private FmodManager FM;
    [SerializeField] public PlayerController Player;
    [SerializeField] public DeathDialogBigJump[] DialogText;
    [SerializeField] public GenericEvent DialogAudio;
    [SerializeField] private int indexScene;
    [SerializeField] public int deathCount;
    [SerializeField] public float TimeBindMegaJump = 2.5f;
    [SerializeField] public bool readyToBigJump = false;
    [SerializeField] public GameObject Fence;
    private void Start()
    {
        FM = FmodManager.instance;
        FM.CreateGenericEventInstance(ref DialogAudio);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        Player = collision.gameObject.GetComponent<PlayerController>();
        if (Player)
        {
            if (readyToBigJump) return;
            Sequence jump = DOTween.Sequence();
            FM.ChangeParameterInt(DialogAudio, "deathcount", deathCount);
            switch (deathCount)
            {
                case 2:
                    DialogText[0].StartTextline();//are you even trying?
                    FM.StartEvent(DialogAudio);
                    break;

                case 4:
                    DialogText[1].StartTextline();//ho something fucked up someone fix. Igotit!

                    DialogAudio.fmodEvent.keyOff();

                    jump.AppendInterval(TimeBindMegaJump).OnComplete(() =>
                    {
                        DialogAudio.fmodEvent.keyOff();
                        Fence.SetActive(false);
                        DialogText[3].StartTextline();//okay try jumping!
                        BindJump();
                        readyToBigJump = true;
                        //gameObject.SetActive(false);
                    });
                    //Debug.Log(jump.Elapsed());
                    break;

                case 8:
                    //DOTween.Sequence().AppendInterval(5f).OnComplete(()=> );//stay still interrupt case 4
                    DialogText[2].StartTextline();
                    //Debug.Log(jump.Elapsed());
                    jump.Pause();
                    jump.Restart();
                    Fence.SetActive(true);
                    //DialogText[3].StartTextline();//stay still interrupt case 4
                    break;

                default:
                    break;
            }

        }
    }
    private void BindJump()
    {
        Player.Jumped -= Player.GetComponent<PlayerAnimator>().OnJumped;
        Player.Jumped += Player_Jumped;
        Player.GetComponent<PlayerAnimator>()._player.Jumped += Player.GetComponent<PlayerAnimator>().OnMegaJumped;
    }

    private void Player_Jumped()
    {
        Player.Stats = statsToSet;
        //Camera.main.transform.parent = null;
        //Camera.main.GetComponent<CameraShake>().UpdateLast(Camera.main.transform.position);
        //Camera.main.GetComponent<CameraShake>().CustomPos = true;
        //DOTween.Sequence().AppendInterval(1f).OnComplete(()=> Camera.main.GetComponent<CameraShake>().StartShake());
        StartCoroutine(ChangeScene(indexScene));
    }

    IEnumerator ChangeScene(int indexScene)
    {

        yield return new WaitForSeconds(3f);

        //FM = FindObjectOfType<FmodManager>();
        //FM.GetComponent<BackgroundMusic>().StartShootemup();

        SceneManager.LoadScene(indexScene);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        BoxCollider2D t = GetComponent<BoxCollider2D>();
        DrawBox(transform.position + new Vector3(t.offset.x, t.offset.y, 0), t.size * transform.localScale, Color.magenta);
    }
    void DrawBox(Vector3 pos, Vector3 size, Color color)
    {
        Handles.color = color;
        Handles.DrawWireCube(pos, size);
    }
#endif
}