using DG.Tweening;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    [SerializeField]
    public FmodManager manager;
    [SerializeField]
    private GenericEvent _platformMusic;
    [SerializeField]
    private GenericEvent _shootemupMusic;
    [SerializeField]
    private GenericEvent _cappellaMusic;
    [SerializeField]
    private GenericEvent _endfallMusic;
    [SerializeField]
    public GenericEvent _meteor;

    [SerializeField]
    public GenericEvent _musicToChangeInto;


    public bool _platform = false;
    public bool _shootemup = false;
    private void Start()
    {
        manager = GetComponent<FmodManager>();
        manager.CreateGenericEventInstance(ref _platformMusic);
        manager.CreateGenericEventInstance(ref _shootemupMusic);
        if (_platform)
        {
            //manager.StopEvent(_shootemupMusic.fmodEvent, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            manager.StartEvent(_platformMusic);
        }
        else if (_shootemup)
        {
            //manager.StopEvent(_platformMusic.fmodEvent, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            manager.StartEvent(_shootemupMusic);
        }
        else
        {
            GenericEvent[] music = new GenericEvent[2];
            music[0] = _platformMusic;
            music[1] = _shootemupMusic;
            StopMusic(music, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }
    public void StopMusic(GenericEvent[] toStop, FMOD.Studio.STOP_MODE StopMode = FMOD.Studio.STOP_MODE.IMMEDIATE)
    {
        for (int i = 0; i < toStop.Length; i++)
        {
            manager.StopEvent(toStop[i].fmodEvent, StopMode);
        }
    }
    public void StopPlatformMusic(FMOD.Studio.STOP_MODE StopMode = FMOD.Studio.STOP_MODE.IMMEDIATE)
    {
        manager.StopEvent(_platformMusic.fmodEvent, StopMode);
    }
    public void StartSnapshot(GenericEvent Temp)
    {
        _musicToChangeInto = Temp;
        manager.CreateGenericEventInstance(ref _musicToChangeInto);
        manager.StartEvent(_musicToChangeInto);
    }
    public void Cappella(bool toggle)
    {
        if (toggle)
        {
            manager.CreateGenericEventInstance(ref _cappellaMusic);
            manager.StartEvent(_cappellaMusic);
        }
        else
        {
            //if (manager.IsPlaying(_cappellaMusic.fmodEvent))
            //{
            //}
            manager.StopEvent(_cappellaMusic.fmodEvent, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            //if (!manager.IsPlaying(_cappellaMusic.fmodEvent))
            //{
            manager.StartEvent(_platformMusic);
            //}
        }
    }
    public void StartEndFall()
    {
        GenericEvent[] music = new GenericEvent[2];
        music[0] = _platformMusic;
        music[1] = _musicToChangeInto;
        StopMusic(music, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

        manager.CreateGenericEventInstance(ref _endfallMusic);
        manager.StartEvent(_endfallMusic);
        //manager.SetEventVolume(_shootemupMusic.fmodEvent,0.05);
    }

    public void StartShootemup()
    {
        manager.StopEvent(_platformMusic.fmodEvent, FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        manager.CreateGenericEventInstance(ref _shootemupMusic);
        manager.StartEvent(_shootemupMusic);
        //manager.SetEventVolume(_shootemupMusic.fmodEvent,0.05);
    }
    public void EndShootemup()
    {
        manager.ChangeParameterInt(_shootemupMusic, "ShootTransition", 1);
        manager.CreateGenericEventInstance(ref _platformMusic);
        DOTween.Sequence().AppendInterval(16.5f).OnComplete(() => _shootemupMusic.fmodEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT));
        DOTween.Sequence().AppendInterval(16.5f).OnComplete(() => manager.StartEvent(_platformMusic));
        //DOTween.Sequence().AppendInterval(16.5f).OnComplete(() => manager.StartEvent(_platformMusic)); questo ci servira' nel futuro prossimo
    }
}
