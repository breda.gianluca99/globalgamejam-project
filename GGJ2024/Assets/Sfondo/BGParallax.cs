using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGParallax : MonoBehaviour
{
    [Tooltip("From nearest to furthest.")]
    [SerializeField] private List<GameObject> layers = new List<GameObject>();
    [SerializeField] private float parallaxSpeedMult = 0.8f;

    GoodCamera goodCamera;
    Vector3 playerSavedPos;


    private void Start()
    {
        goodCamera = FindObjectOfType<GoodCamera>();
    }

    private void Update()
    {
        Vector3 playerPos = goodCamera.transform.position;
        Vector3 deltaPos = playerPos - playerSavedPos;
        playerSavedPos = playerPos;
        Vector3 myDelta = new Vector3();

        for (int i = 0; i < layers.Count; i++)
        {
            int j = i + 1;
            myDelta = Mathf.Pow(parallaxSpeedMult, j) * deltaPos;
            layers[i].transform.position = new Vector3(layers[i].transform.position.x - myDelta.x, layers[i].transform.position.y - myDelta.y, layers[i].transform.position.z);
        }
        //Debug.Log(myDelta);
    }
}
